phinx-sugar
===========


This is a library that try to make phinx migrations more readable, just like Laravel Eloquent.

TODO: Continue.


## Instalation.

You can install phinx-sugar via composer. Just add it add dependency to the ``composer.json`` file.

```javascript
"require": {
	"anyeloamt/phing-sugar": "dev-master"
}

```

Then execute `composer update`.

## phinx-sugar migration example.

```php
<?php namespace PhinxSugar;

use PhinxSugar\Db\Schema;
use PhinxSugar\Migration\Migration;

/**
 * Example migration class
 *
 */
class CreatePostsTable extends Migration
{

    public function up(Schema $schema)
    {
        $table = $schema->create('posts');

        // $table->primaryKey('post_id');

        $table->string('title')
            ->lenght(100)
            ->default('Test title')
            ->nullable(false)
            ->unique();

        $table->text('content');

        $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

        $table->index(array('title'));

        $table->timestamps();
    }

    public function down(Schema $schema)
    {
        $schema->drop('posts');
    }
}

```

## Important

This package is under development and not working yet.