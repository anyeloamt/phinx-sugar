<?php namespace PhinxSugar;

use PhinxSugar\Db\Schema;
use PhinxSugar\Migration\Migration;

/**
 * Example migration class
 *
 */
class CreatePostsTable extends Migration
{

    public function up(Schema $schema)
    {
        $table = $schema->create('posts');

        // $table->primaryKey('post_id');

        $table->string('title')
            ->lenght(100)
            ->default('Test title')
            ->nullable(false)
            ->unique();

        $table->text('content');

        $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');

        $table->index(array('title'));

        $table->timestamps();
    }

    public function down(Schema $schema)
    {
        $schema->drop('posts');
    }
}