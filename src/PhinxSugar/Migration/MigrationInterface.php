<?php namespace PhinxSugar\Migration;

use PhinxSugar\Db\Schema;

/**
 * Interface MigrationInterface
 * @package PhinxSugar\Migration
 *
 * @author Anyelo Almánzar Mercedes <anyeloamt@gmail.com>
 */
interface MigrationInterface
{

    /**
     * Runs the migration
     *
     * @param Schema $schema
     * @return mixed
     */
    public function up(Schema $schema);

    /**
     * Reverse the migration
     *
     * @param Schema $schema
     * @return mixed
     */
    public function down(Schema $schema);

    /**
     * Its executed after the up method
     *
     * @param Schema $schema
     * @return mixed
     */
    public function afterUp(Schema $schema = null);

    /**
     * Its executed after the down method
     *
     * @param Schema $schema
     * @return mixed
     */
    public function afterDown(Schema $schema = null);
} 