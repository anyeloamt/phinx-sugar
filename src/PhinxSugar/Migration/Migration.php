<?php namespace PhinxSugar\Migration;

use PhinxSugar\Db\Schema;

/**
 * Migration base class. All migrations must subclass this.
 *
 * @package PhinxSugar\Migration
 *
 * @author Anyelo Almánzar Mercedes <anyeloamt@gmail.com>
 */
abstract class Migration implements MigrationInterface
{

    /**
     * Returns the timestamps column names.
     * Override this method to change the names. Ex: array('create' => 'reated_at', 'update' => 'updated_at')
     * @return array
     */
    protected function getTimestampsNames()
    {
        return array('update' => 'updated', 'create' => 'created');
    }

    public function afterUp(Schema $schema = null)
    {

    }

    public function afterDown(Schema $schema = null)
    {

    }
} 