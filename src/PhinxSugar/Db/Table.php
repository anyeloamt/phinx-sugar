<?php namespace PhinxSugar\Db;

/**
 * Por ahora sólo tiene los tipos de datos soportados por phinx
 *
 * Class Table
 * @package PhinxWrapper\Db
 *
 * TODO: Finish documentation.
 */
class Table
{
    /**
     * @param $columnName
     * @return Column $column
     */
    public function column($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function primaryKey($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function string($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function text($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function integer($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function bigInteger($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function float($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function decimal($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function dateTime($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function timestamp($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function time($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function date($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function binary($columnName)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function boolean($columnName)
    {

    }

    /**
     * @param $column
     * @return Column $column
     */
    public function foreign($column)
    {

    }

    /**
     * @return void
     */
    public function timestamps()
    {

    }

    /**
     * @param $columns
     */
    public function index($columns)
    {

    }
} 