<?php namespace PhinxSugar\Db;

/**
 * Class Column
 * @package PhinxSugar\Db
 *
 * @method Column default(string $defaultValue)
 *
 * TODO: Most of these methods can be implemented with __call.
 */
class Column
{

    /**
     * @return Column $column
     */
    public function index()
    {

    }

    /**
     * @return Column $column
     */
    public function unique()
    {

    }

    /**
     * @param $limit
     * @return Column $column
     */
    public function limit($limit)
    {

    }

    /**
     * @param $columnName
     * @return Column $column
     */
    public function after($columnName)
    {

    }

    /**
     * @param $referenceName
     * @return Column $column
     */
    public function references($referenceName)
    {

    }

    /**
     * @param $tableName
     * @return Column $column
     */
    public function on($tableName)
    {

    }

    /**
     * @param $from
     * @param $to
     * @return Column $column
     */
    public function rename($from, $to)
    {

    }

    /**
     * @param $value
     * @return Column $column
     */
    public function lenght($value)
    {

    }

    /**
     * @param boolean $value
     * @return Column $column
     */
    public function nullable($value)
    {

    }

    /**
     * @param $value
     * @return Column $column
     */
    public function precision($value)
    {

    }

    /**
     * @param $value
     * @return Column $column
     */
    public function scale($value)
    {

    }

    /**
     * @param $value
     * @return Column $column
     */
    public function update($value)
    {

    }

    /**
     * @param $value
     * @return Column $column
     */
    public function comment($value)
    {

    }

    /**
     * @param $string
     * @return Column $column
     */
    public function onDelete($string)
    {

    }

    /**
     * @param $string
     * @return Column $column
     */
    public function onUpdate($string)
    {

    }

    /**
     * @param $defaultValue
     * @return Column $column
     */
    protected  function _default($defaultValue)
    {

    }

    function __call($name, $arguments)
    {
        if ($name === "default") {

            $this->_default($arguments);
        }
    }

} 