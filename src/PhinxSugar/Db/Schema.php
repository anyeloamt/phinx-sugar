<?php namespace PhinxSugar\Db;

/**
 * Class Schema
 * @package PhinxSugar\Db
 *
 * TODO: Finish documentation
 */
class Schema
{
    /**
     * @param $tableName
     * @return Table $table
     */
    public function table($tableName)
    {

    }

    /**
     * @param $tableName
     * @return Table $table
     */
    public function create($tableName)
    {

    }

    /**
     * @param $tableName
     */
    public function drop($tableName)
    {

    }

    /**
     * @param $tableName
     */
    public function dropIfexists($tableName)
    {

    }
} 